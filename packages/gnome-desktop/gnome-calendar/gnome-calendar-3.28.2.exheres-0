# Copyright © 2012 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require freedesktop-desktop gsettings gtk-icon-cache
require meson [ meson_minimum_version=0.42 ]

SUMMARY="A simple calendar for GNOME 3"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gtk-doc
"

DEPENDENCIES="
    build:
        sys-devel/gettext
        virtual/pkg-config
        gtk-doc? ( dev-doc/gtk-doc )
    build+run:
        dev-libs/glib:2[>=2.43.4]
        dev-libs/libdazzle:1.0[>=3.26.1]
        gnome-desktop/evolution-data-server:1.2[>=3.17.1][gtk]
        gnome-desktop/geocode-glib:1.0[>=3.23]
        gnome-desktop/gnome-online-accounts[>=3.2.0]
        gnome-desktop/gsettings-desktop-schemas[>=3.21.2]
        gnome-desktop/libgweather:3.0[>=3.27.2]
        gnome-desktop/libsoup:2.4
        gps/geoclue:2.0[>=2.4]
        office-libs/libical:=[>=1.0]
        x11-libs/gtk+:3[>=3.22.20]
"

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gtk-doc enable-gtk-doc'
)

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

