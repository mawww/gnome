# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Purpose License v2

require gnome.org [ suffix=tar.xz ]
require gsettings
require vala [ with_opt=true vala_dep=true ]
require cmake [ api=2 ]
require test-dbus-daemon

SUMMARY="Backend store for contact data"
HOMEPAGE="http://projects.gnome.org/evolution"

LICENCES="LGPL-2"
SLOT="1.2"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gobject-introspection
    google [[ description = [ build google addressbook backend ] ]]
    gtk [[ description = [ build GTK+ based prompting service ] ]]
    kerberos [[ description = [ Enable kerberos authentication support ] ]]
    ldap [[ description = [ Enable LDAP support in evolution ] ]]
    vapi [[
        description = [ Build Vala bindings ]
        requires = [ gobject-introspection ]
    ]]
    weather  [[ description = [ Enable weather calendar component ] ]]
    ( linguas: am ar as ast az be bg bn bn_IN bs ca ca@valencia cs cy da de dz el en_AU en_CA en_GB
               en@shaw eo es et eu fa fi fr ga gl gu he hi hr hu id is it ja ka km kn ko ku lt lv
               mai mk ml mn mr ms nb ne nl nn oc or pa pl pt pt_BR ro ru rw si sk sl sq sr sr@latin
               sv ta te tg th tr ug uk vi wa xh zh_CN zh_HK zh_TW )
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.20]
        dev-lang/python:*
    build+run:
        dev-libs/glib:2[>=2.46]
        dev-libs/libxml2:2.0[>=2.0.0]
        gnome-desktop/libsoup:2.4[>=2.42]
        office-libs/libical[>=2.0.0]
        gnome-desktop/gnome-online-accounts[>=3.8]
        dev-libs/libsecret:1[>=0.5]
        gnome-desktop/gcr[>=3.4]
        sys-libs/db:=
        dev-libs/nspr
        dev-libs/nss
        dev-db/sqlite:3[>=3.7.17]
        dev-util/gperf
        dev-libs/icu:=
        media-libs/libcanberra[>=0.25]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.12] )
        google? (
            core/json-glib[>=1.0.4]
            gnome-desktop/libgdata[>=0.15.1]
            net-libs/webkit:4.0[>=2.11.91]
        )
        gtk? ( x11-libs/gtk+:3[>=3.10.0] )
        kerberos? ( app-crypt/heimdal )
        ldap? ( net-directory/openldap[>=2.4.0] )
        weather? ( gnome-desktop/libgweather:3.0[>=3.10] )
"

AT_M4DIR=( m4 )

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DSHARE_INSTALL_PREFIX:STRING=/usr/share
    -DSYSCONF_INSTALL_DIR:STRING=/etc
    -DENABLE_IPV6:BOOL=TRUE
    -DENABLE_GOA:BOOL=TRUE
    -DENABLE_LARGEFILE:BOOL=TRUE
    -DENABLE_SMIME:BOOL=TRUE
    -DWITH_SUNLDAP:BOOL=FALSE
    -DENABLE_BACKTRACES:BOOL=TRUE
    -DENABLE_UOA:BOOL=FALSE
    -DENABLE_EXAMPLES:BOOL=FALSE
    -DENABLE_BACKEND_PER_PROCESS:BOOL=TRUE
    -DENABLE_CANBERRA:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTIONS=(
    'gobject-introspection ENABLE_INTROSPECTION'
    'google ENABLE_GOOGLE'
    'google ENABLE_OAUTH2'
    'gtk ENABLE_GTK'
    'kerberos WITH_KRB5'
    'ldap WITH_OPENLDAP'
    'vapi ENABLE_VALA_BINDINGS'
    'weather ENABLE_WEATHER'
)

src_test() {
    test-dbus-daemon_src_test
}

