# Copyright 2009 Jonathan Dahan <jedahan@gmail.com>
# Copyright 2011 Brett Witherspoon <spoonb@exherbo.org>
# Copyright 2015 Volodymyr Medvid <vmedvid@riseup.net>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] gsettings python [ blacklist=2 multibuild=false ]
require vala [ vala_dep=true with_opt=true option_name=findinfiles ]

SUMMARY="Additional plugins for gedit"
DESCRIPTION="
Currently available plugins:
    Bookmarks - Easy document navigation with bookmarks,
    Bracket Completion - Automatically adds closing brackets,
    Character Map - Insert special characters just by clicking on them,
    Code Comment - Comment out or uncomment a selected block of code,
    Color Picker - Pick a color from a dialog and insert its hexadecimal representation,
    Color Scheme Editor - Source code color scheme editor,
    Commander - Command line interface for advanced editing,
    Dashboard - A Dashboard for new tabs,
    Draw Spaces - Draw spaces and tabs,
    Find in Files - Find text in all files of a folder,
    Git - Highlight lines that have been changed since the last commit,
    Join/Split Lines - Join several lines or split long ones,
    Multi Edit - Edit document in multiple places at once,
    Smart Spaces - Forget you're not using tabulations,
    SyncTeX - Synchronize between LaTeX and PDF with gedit and evince,
    Embedded Terminal - Embed a terminal in the bottom pane,
    Text Size - Easily increase and decrease the text size,
    Word Completion - Word completion using the completion framework,
    Zeitgeist dataprovider - Logs access and leave event for documents used with gedit
"
HOMEPAGE="https://live.gnome.org/Gedit/Plugins"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    charmap     [[ description = [ Enable the Character Map plugin ] ]]
    findinfiles [[ description = [ Enable the Find in Files plugin ] ]]
    git         [[ description = [ Enable the Git plugin ] ]]
    synctex     [[ description = [ Enable the SyncTeX plugin ] ]]
    terminal    [[ description = [ Enable the Embedded Terminal plugin ] ]]
    zeitgeist   [[ description = [ Enable the Zeitgeist dataprovider and Dashboard plugins ] ]]
"

DEPENDENCIES="
    build:
        dev-util/intltool[>=0.50.2]
        gnome-desktop/yelp-tools
        virtual/pkg-config
        sys-devel/gettext[>=0.17]
    build+run:
        app-editors/gedit[>=3.16.0][python]
        dev-libs/glib:2[>=2.32.0]
        dev-libs/libpeas:1.0[>=1.7.0][python_abis:*(-)?]
        dev-python/dbus-python[>=0.82][python_abis:*(-)?]
        gnome-desktop/gtksourceview:3.0[>=3.21.3][gobject-introspection]
        x11-libs/gtk+:3[>=3.9.0][gobject-introspection]
        charmap? ( gnome-extra/gucharmap[gobject-introspection] )
        git? ( dev-scm/libgit2-glib[>=0.0.6] )
        synctex? ( dev-python/dbus-python[>=0.82]
                   gnome-bindings/pygobject )
        terminal? ( dev-libs/vte:2.91[gobject-introspection] )
        zeitgeist? ( dev-libs/zeitgeist:2.0[>=0.9.12][gobject-introspection] )
"

BUGS_TO="jedahan@gmail.com spoonb@exherbo.org"

# Most plugins depend on python, there is no reason to disable it
DEFAULT_SRC_CONFIGURE_PARAMS=( --enable-python )

# TODO(medvid) for now, leave automagic on gucharmap, vte and libgit2-glib
# On runtime, plugins are disabled by default, so that's not a big deal
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    zeitgeist
    'findinfiles vala'
)

