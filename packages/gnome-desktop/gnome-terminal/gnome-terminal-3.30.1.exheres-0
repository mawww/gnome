# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2017-2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] gsettings
SUMMARY="Terminal Application for the GNOME Desktop"
HOMEPAGE="https://www.gnome.org/"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    nautilus-extension [[ description = [ build nautilus extension ] ]]
    gnome-shell [[ description = [ build gnome-shell search provider ] ]]
"

DEPENDENCIES="
    build:
        dev-lang/perl:*[>=5.8.1]
        dev-lang/vala:*[>=0.26] [[ note = [ We need the m4 file for the autotools ] ]]
        dev-libs/libxml2:2.0 [[ note = [ required for xmllint ] ]]
        dev-util/desktop-file-utils
        dev-util/intltool[>=0.50.0]
        gnome-desktop/yelp-tools
        sys-devel/gettext
        virtual/pkg-config[>=0.20]
        gnome-shell? ( gnome-desktop/gnome-shell )
    build+run:
        dev-libs/vte:2.91[>=0.54.1][gnutls]
        dev-libs/glib:2[>=2.42.0]
        dev-libs/pcre2[>=10.0]
        gnome-desktop/gsettings-desktop-schemas[>=0.1.0]
        gnome-desktop/dconf[>=0.14]
        sys-apps/util-linux [[ note = [ provides uuid ] ]]
        x11-libs/gtk+:3[>=3.22.0]
        nautilus-extension? ( gnome-desktop/nautilus[>=3.0.0] )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    '--enable-distro-packaging'
    '--disable-gterminal'
    '--disable-migration'
    '--with-gtk=3.0'
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( nautilus-extension )
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( 'gnome-shell search-provider' )

