# Copyright 2014 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] gsettings meson

SUMMARY="A log viewer for the systemd journal"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"

MYOPTIONS=""

DEPENDENCIES="
    build:
        app-text/docbook-xml-dtd:4.3
        app-text/docbook-xsl-stylesheets
        dev-libs/libxslt
        gnome-desktop/yelp-tools
        sys-devel/gettext
        virtual/pkg-config[>=0.24]
    build+run:
        dev-libs/glib:2[>=2.43.90]
        gnome-desktop/gsettings-desktop-schemas
        sys-apps/systemd
        x11-libs/gtk+:3[>=3.22.0]
    test:
        dev-util/desktop-file-utils
"

# Require access to log facilities ( e.g. journald )
RESTRICT="test"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dman=true
    -Dtests=false
)

pkg_setup() {
    edo mkdir "${WORKBASE}/bin"
    edo ln -s /usr/$(exhost --build)/bin/${PKG_CONFIG} "${WORKBASE}"/bin/pkg-config
    export PATH="${WORKBASE}/bin:$PATH"
}

