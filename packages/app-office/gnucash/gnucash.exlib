# Copyright 2009-2013 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org gsettings

export_exlib_phases src_configure src_test src_install

SUMMARY="GnuCash is personal and small-business financial-accounting software"
DESCRIPTION="
GnuCash allows you to track bank accounts, stocks, income, and expenses. As quick
and intuitive to use as a checkbook register, it is based on professional accounting
principles to ensure balanced books and accurate reports. It is backed by an active
development community and is blossoming into a full-fledged accounting system.
"
HOMEPAGE="http://${PN}.org"
DOWNLOADS="mirror://sourceforge/${PN}/${PNV}.tar.bz2"

REMOTE_IDS="freecode:${PN} sourceforge:${PN}"

UPSTREAM_DOCUMENTATION="${HOMEPAGE}/docs.phtml [[ lang = en ]]"

SLOT="0"
LICENCES="GPL-2"

MYOPTIONS="debug ofx
    aqbanking [[ description = [ Enable HBCI/AqBanking support, for connecting to some internet banks ] ]]
    chipcard  [[ description = [ Enable support for chipcard reading and processing ] ]]
    dbi       [[ description = [ Enable SQLite/MySQL/PostgreSQL support via libdbi ] ]]
    ( linguas: ar as bg brx ca cs da de doi el en_GB es_NI es eu fa fi fr gu he hi hu it ja kn ko
               kok kok@latin ks lt lv mr nb ne nl pl pt_BR pt ro ru rw sk sv ta te tr uk ur vi zh_CN
               zh_TW )
"

DEPENDENCIES="
    build:
        dev-libs/boost
        dev-util/intltool
        virtual/pkg-config
    build+run:
        dev-lang/guile:2.0[>=2.0.0]
        dev-libs/glib:2[>=2.38.0]
        dev-libs/libsecret[>=0.18]
        dev-libs/libxml2:2.0[>=2.6.32]
        dev-libs/libxslt
        dev-scheme/slib[>=3.2.1]
        gnome-platform/libgnomecanvas
        net-libs/webkit-gtk:1[>=1.2]
        office-libs/goffice:0.8[>=0.8.14]
        x11-libs/gdk-pixbuf:2.0
        x11-libs/gtk+:2[>=2.24.0]
        aqbanking? (
            dev-libs/aqbanking[>=3.8.2] [[ description = [ qt3 support must be enabled ] ]]
            chipcard? ( dev-libs/libchipcard[>=4.2.7] )
        )
        dbi? ( dev-libs/libdbi[>=0.8.3] )
        ofx? ( finance/libofx[>=0.7.0] )
    run:
        gnome-desktop/dconf
"

# The tests don't respect the configuration choices but try to test even stuff
# that's been explicitly disabled.
RESTRICT="test"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-gui
    --enable-locale-specific-tax
    --enable-nls
    --disable-dot
    --disable-doxygen
    --disable-efence
    --disable-error-on-warning
    --disable-html-docs
    --disable-latex-docs
    --disable-python
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    aqbanking
    dbi
    debug
    ofx
)

DEFAULT_SRC_INSTALL_PARAMS=(
    GNC_DOC_INSTALL_DIR=/usr/share/doc/${PNVR}
)

gnucash_src_configure() {
    export CPPFLAGS=${CXXFLAGS}
    export GUILE="guile-2.0"

    default
}

gnucash_src_test() {
    GUILE_WARN_DEPRECATED=no \
    GNC_DOT_DIR="${TEMP}"/.gnucash \
    emake -j1 check
}

gnucash_src_install() {
    gsettings_src_install
    keepdir /usr/share/gnucash/guile-modules/srfi

    # Clean up
    edo rm "${IMAGE}"/usr/share/doc/${PNVR}/{COPYING,INSTALL,LICENSE,*win32-bin.txt}
}

